import app from 'firebase/app';
import 'firebase/auth';


const config = {
  apiKey: "AIzaSyDp2Ctti_qpkrze4jUgpEGiusMml0nbTTo",
  authDomain: "todolist-935c1.firebaseapp.com",
  databaseURL: "https://todolist-935c1.firebaseio.com",
  projectId: "todolist-935c1",
  storageBucket: "todolist-935c1.appspot.com",
  messagingSenderId: "28353393818",
  appId: "1:28353393818:web:801393b581d8a89a621b23",
  measurementId: "G-ZS3SG5G3EH"
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
  }
  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = (email) => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = (password) =>
    this.auth.currentUser.updatePassword(password);
}

export default Firebase;